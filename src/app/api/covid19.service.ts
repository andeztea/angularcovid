import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';




@Injectable({
  providedIn: 'root'
})
export class Covid19Service {

  constructor(private http: HttpClient) { }

  getIndonesia(){
    return this.http.get('https://kawalcovid19.harippe.id/api/summary');
  }



}
