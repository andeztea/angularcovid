import { Component } from '@angular/core';
import {Covid19Service} from '../app/api/covid19.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'angulargw';
  confirmed: any;
  activeCare: any;
  deaths: any;
  recovered: any;
  metadata: any;
  Death: any;
  constructor(public covid19: Covid19Service,){
    this.showIndonesia();

  }

  
  showIndonesia() {

    this.covid19.getIndonesia().subscribe((data: any[])=>{
      this.confirmed=data['confirmed'].value;
      this.activeCare=data['activeCare'].value;
      this.recovered=data['recovered'].value;
      this.deaths=data['deaths'].value;
      this.metadata=data['metadata']['lastUpdatedAt'];
    })  


   
  }





}
